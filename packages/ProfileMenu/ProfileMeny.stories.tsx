// Footer.stories.ts|tsx

import React from 'react'

import { ComponentMeta } from '@storybook/react'
import { ProfileMenu } from './src'
import { ProfileMenuProps } from './src/components'

export default {
  title: 'ProfileMenu',
  component: ProfileMenu,
  parameters: {
    layout: 'centered',
  },
} as ComponentMeta<typeof ProfileMenu>

const authorizedProps: ProfileMenuProps = {
  user: {
    contractsAmount: 8,
    ordersAmount: 100,
    bonuses: 200,
    firstName: 'Михаил',
    lastName: 'Александров',
    is_loyalty_registered: false,
  },
}

export const AuthorizedIcon = () => <ProfileMenu {...authorizedProps} />
