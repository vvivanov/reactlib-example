import React, { FC } from 'react'
import Popup from 'reactjs-popup'
import { ProfileIcon } from '../ProfileIcon'
import { RequiredProfileMenuProps } from '../index'
import styled from 'styled-components'
import { Content } from './content'

const StyledPopup = styled(Popup)`
  &-content {
    margin: auto;
    background: #fff;
    max-width: 360px;
    width: 100%;
    padding: 5px;
    border-radius: 4px;
    filter: drop-shadow(0px 0px 30px rgba(0, 0, 0, 0.1));
  }

  [role='tooltip'].popup-content {
    width: 200px;
    box-shadow: 0 0 3px rgba(0, 0, 0, 0.16);
    border-radius: 5px;
  }

  &-overlay {
    background: rgba(0, 0, 0, 0.5);
  }

  [data-popup='tooltip'].popup-overlay {
    background: transparent;
  }

  &-arrow {
    color: #fff;
    stroke-dasharray: 30px;
    stroke-dashoffset: -54px;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
  }
`

type Props = RequiredProfileMenuProps

export const PopoverMenu: FC<Props> = (props) => {
  return (
    <StyledPopup
      // open
      on={['hover']}
      arrow
      position='bottom center'
      closeOnDocumentClick
      trigger={
        <div style={{ display: 'inline-block' }}>
          <ProfileIcon {...props} />
        </div>
      }
    >
      <Content {...props} />
    </StyledPopup>
  )
}
