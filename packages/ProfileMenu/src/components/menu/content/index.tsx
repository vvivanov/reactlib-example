import React, { FC } from 'react'
import { MenuButtons } from './MenuButtons'
import { ProfileInfo } from './ProfileInfo'
import { RequiredProfileMenuProps } from '../../index'

type Props = RequiredProfileMenuProps

const concatNonNullValues = (...values: Array<string | undefined>): string => {
  const filledText = values.filter(Boolean)
  return filledText.join(' ')
}

export const Content: FC<Props> = (props) => {
  const { user } = props
  return (
    <div>
      <ProfileInfo fullName={concatNonNullValues(user?.firstName, user?.lastName)} />
      <MenuButtons {...props} />
    </div>
  )
}
