/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';
import {TyreCalculator} from "./components";

test('renders footer', () => {
  const { getByText } = render(<TyreCalculator />);
  const linkElement = getByText(/tyrecalculator/i);
  expect(linkElement).toBeDefined();
});
