import {TyreUsedFacetFilterDto} from "./TyreUsedFacetFilterDto";

export type TyreUsedFacetDataResponse = {
  items: {
    id: number;
    width: string;
    profile: string;
    diameter: string;
  }[];
  limit: number;
  offset: number;
  total: number;
}


export type CommonResponseObject = {
  status_code: number;
  meta: any;
}

export const createApi = (apiUrl: string = '') => {
  const getTyreUsedFacet = async (): Promise<TyreUsedFacetWrapperResponse> => {
    return await fetch(`${apiUrl}/v1/offer/tyre-used-facet`).then(
      async (response) => await response.json(),
    )
  }

  const getTyreUsedFacetFiltered = async (dto: TyreUsedFacetFilterDto): Promise<TyreUsedFacetFilteredWrapperResponse> => {
    let filterString = '';

    if (dto?.profile) {
      filterString = filterString + `profile=${dto.profile}`;
    }
    if (dto?.diameter) {
      filterString = filterString ? `$${filterString}` : filterString;
      filterString = filterString + `diameter=${dto.diameter}`;
    }
    if (dto?.width) {
      filterString = filterString ? `$${filterString}` : filterString;
      filterString = filterString + `width=${dto.width}`;
    }
    filterString = filterString ? `?${filterString}` : filterString;
    return await fetch(`${apiUrl}/v1/offer/tyre-used-facet-filter${filterString}`).then(
      async (response) => await response.json(),
    )
  }

  const getTyreUsedFacetCalculatedPrice = async (dto: TyreUsedFacetFilterDto): Promise<TyreUsedFacetCalculatedPriceWrapperResponse> => {
    let obj: TyreUsedFacetFilterDto = {}
    
    if (dto?.profile) {
      obj.profile = dto.profile;
    }
    if (dto?.diameter) {
      obj.diameter = dto.diameter;
    }
    if (dto?.width) {
      obj.width = dto.width;
    }

    // return await fetch(`${apiUrl}/v1/offer/tyre-used-facet-calc`, {
    //   method: "POST",
    //   headers: {mode: 'no-cors', 'Content-Type': 'application/json'},
    //   body: JSON.stringify(obj),
    // }).then(
    //   async (response) => await response.json(),
    // )
    //
    const requestOptions = {
      method: 'POST',
      // headers: {'Content-Type': 'application/json'},

      headers: {
        'Content-Type': 'application/json',
        credentials: 'same-origin',
      },
      
      body: JSON.stringify(obj)
    };
    return await fetch(`${apiUrl}/v1/offer/tyre-used-facet-calc`, requestOptions).then(
      async (response) => await response.json(),
    )
  }

  return {
    getTyreUsedFacet: getTyreUsedFacet,
    getTyreUsedFacetFiltered: getTyreUsedFacetFiltered,
    getTyreUsedFacetCalculatedPrice: getTyreUsedFacetCalculatedPrice,
  }
}
export type TyreUsedFacetWrapperResponse = {
  status_code: number;
  meta: any;
  data: TyreUsedFacetDataResponse;
}

export type TyreUsedFacetFilteredWrapperResponse = {
  status_code: number;
  meta: any;
  data: TyreUsedFacetFilteredDataResponse;
}

export type TyreUsedFacetFilteredDataResponse = {
  profile?: string[],
  diameter?: string[],
  width?: number[],
}

export type TyreUsedFacetCalculatedPriceWrapperResponse = {
  status_code: number;
  meta: any;
  data: {
    price: number;
  };
  errors: any;
}