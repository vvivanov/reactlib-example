import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import { TyreCalculator } from './src';

export default {
    title: 'Tyre Calculator',
    component: TyreCalculator,
} as ComponentMeta<typeof TyreCalculator>;

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof TyreCalculator> = (args) => <TyreCalculator {...args} />;

export const Primary = Template.bind({});

Primary.args = {
    primary: true,
    label: 'Tyre Calculator',
};