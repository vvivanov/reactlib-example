import typescript from "rollup-plugin-typescript2";
import commonjs from "@rollup/plugin-commonjs";
import dts from "rollup-plugin-dts";
import svgr from '@svgr/rollup'

export default [
  // сначала соберем пакет
  {
    input: ["src/index.ts"],
    output: [
      {
        dir: "dist",
        entryFileNames: "[name].common.js",
        format: "cjs",
        exports: "named",
      },
      {
        dir: "dist",
        entryFileNames: "[name].esm.js",
        format: "esm",
      },
    ],
    plugins: [
      typescript(),
      commonjs(),
      svgr()
    ],
    external: ["react", "styled-components"],
  },
  {
    // потом собираем типы
    input: "src/index.ts",
    output: [{ file: "dist/index.d.ts", format: "es" }],
    plugins: [dts()],
  },
];