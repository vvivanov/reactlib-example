import { TAuto, TAutoFacets } from './interfaces'

export const createApi = (apiUrl: string = '') => {
  const loadFacetsByStep = async <T extends keyof TAutoFacets>(
    step: T,
    slug?: string,
  ): Promise<TAutoFacets[T]> => {
    const slugPath = slug ? `/${slug}` : ''
    const entity = step === 'brand' ? '' : `/${step}s`
    return await fetch(`${apiUrl}/api/auto${entity}${slugPath}.json`).then(
      async (response) => await response.json(),
    )
  }

  const loadFacetsByAuto = async (auto: TAuto): Promise<TAutoFacets> => {
    const autoFacetObject: TAutoFacets = {
      brand: null,
      model: null,
      generation: null,
      modification: null,
    }

    const getPromisesCollection = (autoObject: TAuto): Promise<any>[] => {
      const all: Promise<any>[] = [loadFacetsByStep('brand')]
      if (autoObject?.brand) {
        all.push(loadFacetsByStep('model', autoObject?.brand?.slug))
      }
      if (autoObject?.model) {
        all.push(loadFacetsByStep('generation', autoObject.model?.slug))
      }
      if (autoObject?.generation) {
        all.push(loadFacetsByStep('modification', autoObject.generation?.slug))
      }
      return all
    }

    const responses = await Promise.all(getPromisesCollection(auto))

    responses.forEach((response, idx) => {
      switch (idx) {
        case 0:
          autoFacetObject.brand = response ?? null
          break
        case 1:
          autoFacetObject.model = response ?? null
          break
        case 2:
          autoFacetObject.generation = response ?? null
          break
        case 3:
          autoFacetObject.modification = response ?? null
          break
      }
    })
    return autoFacetObject
  }

  return {
    loadFacetsByStep,
    loadFacetsByAuto,
  }
}
