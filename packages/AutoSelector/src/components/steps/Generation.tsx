import React, { FC } from 'react'
import { TGeneration } from '../../interfaces'
import { GenerationListWithPhoto } from './components/GenerationListWithPhoto'
import { useStore } from '../provider/context'

const isSkipModificationPage = (generation: TGeneration): boolean => {
  return generation?.isSubitemsOptional || generation?.itemsCount === 1
}

export const Generation: FC = () => {
  const { facets, setFacets, setAuto, setStep, auto, setIsLoading, baseLink, onClose, api } =
    useStore()

  const onSelect = (generation: TGeneration) => {
    const skipModificationPage = isSkipModificationPage(generation)
    const isSameGenerationSelected =
      generation.slug === auto.generation?.slug && skipModificationPage
    if (isSameGenerationSelected) {
      onClose()
      return
    }

    const updatedAuto = { ...auto, generation, modification: null }

    setAuto(updatedAuto)
    setFacets({ ...facets, modification: null })

    api
      .loadFacetsByStep('modification', generation.slug)
      .then((modification) => {
        setFacets({ ...facets, modification })
        if (skipModificationPage && modification && modification[0]) {
          setAuto({ ...updatedAuto, modification: modification[0] })
        }
      })
      .finally(() => setIsLoading(false))

    if (skipModificationPage) {
      onClose()
    } else {
      setStep('modification')
    }
  }

  return (
    <GenerationListWithPhoto
      baseLink={baseLink}
      data={facets.generation ?? []}
      onSelect={onSelect}
    />
  )
}
