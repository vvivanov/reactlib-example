import React, { FC } from 'react'
import { TModification } from '../../interfaces'
import { ModificationListDetailed } from './components/ModificationListDetailed'
import { useStore } from '../provider/context'

export const Modification: FC = () => {
  const { facets, setAuto, auto, onClose } = useStore()

  const onSelect = (modification: TModification) => {
    if (modification.slug !== auto.modification?.slug) {
      setAuto({ ...auto, modification })
    }
    onClose()
  }

  return <ModificationListDetailed data={facets.modification ?? []} onSelect={onSelect} />
}
