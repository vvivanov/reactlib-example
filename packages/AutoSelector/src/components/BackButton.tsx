import React, { FC } from 'react'
import { StyledBackButton } from './Styled'
import { useStore } from './provider/context'
import { TStep } from '../interfaces'
import BackIcon from '../icons/back-ios.svg'

const ORDER: [TStep, TStep, TStep, TStep] = ['brand', 'model', 'generation', 'modification']

export const BackButton: FC = () => {
  const { setStep, step } = useStore()
  const onBackClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault()
    const currentIndex = ORDER.findIndex((item) => item === step)
    if (currentIndex !== 0) {
      const prevStep = ORDER[currentIndex - 1]
      setStep(prevStep)
    }
  }
  return (
    <StyledBackButton>
      <a href={'#'} onClick={onBackClick}>
        <BackIcon width={6} height={8} /> Назад
      </a>
    </StyledBackButton>
  )
}
